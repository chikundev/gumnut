-- chikun :: 2015
-- Load all libraries


require(... .. "/bindings")		-- Shorthand bindings
require(... .. "/class")		-- Class system
require(... .. "/maths")		-- Extra maths functions
require(... .. "/states")		-- State manager
