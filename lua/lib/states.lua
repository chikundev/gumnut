-- chikun :: 2014-2015
-- Manages different states


-- Create state table
state = {
    current = nil
}


--[[
	Kill old state and load a new one.
	INPUT:	State to change to.
	OUTPUT:	Nothing.
]]
function state.change(new_state)

    state.current:kill()
    state.load(new_state)
end


--[[
	Loads new state. Reloads current state if argument omitted.
	INPUT:	New state to load, or nothing.
	OUTPUT:	Nothing.
]]
function state.load(new_state)

    state.current = new_state or state.current
    state.current:create()
end


--[[
	Kill old state and sets another.
	INPUT:	State to move to.
	OUTPUT:	Nothing.
]]
function state.set(new_state)

    state.current.kill()
    state.current = new_state
end


--[[
	Update current state.
	INPUT:	Delta time.
	OUTPUT:	Nothing.
]]
function state.update(dt)

    state.current:update(dt)
end


--[[
	Draw current state.
	INPUT:	Nothing.
	OUTPUT:	Nothing.
]]
function state.draw()

    state.current:draw()
end
