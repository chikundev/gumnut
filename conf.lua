-- funkeh :: 2015
-- Configuration file for Gumnut

function love.conf(game)

	game.identity = "Gumnut"
	game.version = "0.9.2"
	game.console = true      -- Set to false on distribution

	-- Window arguments
	game.window.title = "Gumnut"
	-- game.window.icon = "gfx/system/icon.png"
	game.window.width = 800
	game.window.height = 480
	game.window.borderless = false
	game.window.resizable = false
	game.window.minwidth = 800
	game.window.minheight = 480
	game.window.fullscreen = false
	game.window.fullscreentype = "desktop"
	game.window.vsync = false
	game.window.fsaa = 0
	game.window.display = 1
	game.window.highdpi = false
	game.window.srgb = true
	game.window.x = nil
	game.window.y = nil

	-- Disable unnecessary modules
	game.modules.physics = false
end
